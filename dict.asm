%include "lib.inc"
global find_word
section .text

;arg rdi, rsi
find_word:
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		je .success
		mov rsi, [rsi]
		cmp rsi, 0
		jne .loop
		jmp .ends

	.success:
		mov rax, rsi
		ret
		
	.ends: 
		mov rax, 0
		ret
