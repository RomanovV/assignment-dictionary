%include "lib.inc"
%include "dict.inc"

section .data
failure_msg: db "Word not found", 0
error_msg: db "Incorrect input values", 0
%define BUFFER_LENGTH 255
%define stderr 2
%define sys_write 1
section .text
%include "words.inc"
print_error:
	xor rax, rax
	call string_length
	mov rdx, rax
	mov rax, sys_write
	mov rsi, rdi
	mov rdi, stderr
	syscall
	ret

read_MyString:
	xor rdx, rdx
	xor rcx, rcx
	.read:
		dec rsi
		cmp rsi, 0
		je .end
		push rdi
		push rdx
		push rsi
		call read_char
		pop rsi
		pop rdx
		pop rdi
		cmp al, 0xA
		je .success
		cmp al, 0
		je .success
		mov [rdi+rdx], rax
		inc rdx
		jmp .read
	.end:
		mov rax, 0
		ret
	.success:
		mov rax, rdi
		ret

global _start
_start:
	sub rsp, BUFFER_LENGTH
	mov rsi, BUFFER_LENGTH
	mov rdi, rsp
	call read_MyString
	cmp rdx, 0
	je .input_invalid
	mov rsi, PREV_LABEL
	call find_word
	cmp rax, 0
	je .failure
	call print_string
	call exit
	.failure:
		mov rdi, failure_msg
		call print_string
		call exit
	.input_invalid:
		mov rdi, error_msg
		call print_error
		call exit


