section .text
section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


%define stdout 1
%define sys_write 1
%define sys_read 0

exit:
	mov rax, 60
	syscall
	
; arg rdi
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret

;arg rdi
print_string:
	call string_length
	mov rdx, rax
	mov rsi, rdi
	mov rax, sys_write
	mov rdi, stdout
	syscall
	ret

;arg rdi
print_char:
	xor rax, rax
	push rdi
	mov rdx, 1
	mov rsi, rsp
	mov rax, sys_write
	mov rdi, stdout
	syscall
	pop rdi
	ret

; done
print_newline:
	xor rax, rax
	mov rdi, 0xA
	jmp print_char

; arg rdi
print_uint:
	xor rax, rax
	dec rsp
	mov [rsp], al
	mov rax, rdi
	mov rcx, 10
	xor r8, r8
	.loop:
		inc r8
		xor rdx, rdx
		div rcx
		add dl, '0'
		dec rsp
		mov [rsp], dl
		cmp rax, 0
		jne .loop
	mov rdi, rsp
	push r8
	call print_string
	pop r8
	add rsp, r8
	inc rsp
	ret

; arg rdi
print_int:
	xor rax, rax
	cmp rdi, 0
	jge .end
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.end:
		jmp print_uint

;arg rdi, rsi, return rax
string_equals:
	xor rax, rax
	xor rcx, rcx
	.loop:
		mov cl, byte[rdi+rax]
		cmp byte[rsi+rax], cl
		jne .endN
		cmp byte[rdi + rax], 0
		je .endE
		inc rax
		jmp .loop
	.endN:
		mov rax, 0
		ret 
	.endE:
		mov rax, 1
		ret 

;return rax
read_char:
	xor rax, rax
	push rax
	mov rdi, sys_read
	mov rdx, sys_write
	mov rsi, rsp
	syscall
	pop rax
	ret

;arg rdi, rsi
read_word:
	xor rdx, rdx
	xor rcx, rcx
	.skip:
		dec rsi
		cmp rsi, 0
		je .end
		push rdi
		push rdx
		push rsi
		call read_char
		pop rsi
		pop rdx
		pop rdi
		cmp al, 0x20
		je .skip
		cmp al, 0x9
		je .skip
		cmp al, 0xA
		je .skip
	.read:
		cmp al, 0
		je .success
		mov [rdi+rdx], rax
		inc rdx
		dec rsi
		cmp rsi, 0
		je .end
		push rdi
		push rdx
		push rsi
		call read_char
		pop rsi
		pop rdx
		pop rdi
		cmp al, 0x20
		je .success
		cmp al, 0x9
		je .success
		cmp al, 0xA
		je .success
		jmp .read
	.end:
		mov rax, 0
		ret
	.success:
		mov rax, rdi
		ret
;arg rdi
parse_uint:
	xor rax, rax
	xor rcx, rcx
	.loop:
		cmp byte[rdi], '0'
		jb .end
		cmp byte[rdi], '9'
		ja .end
		inc rcx
		push rcx
		imul rax, 10
		pop rcx
		add al, byte[rdi]
		sub al, '0'
		inc rdi
		jmp .loop
	.end:
		mov rdx, rcx
		ret
;arg rdi
parse_int:
	xor rax, rax
	xor rdx, rdx
	cmp byte [rdi], '-'
	jne .loop
	push rsi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	pop rsi
	inc rdi
	.loop:
		push rdx
		call parse_uint
		pop rcx
		add rdx, rcx
	ret

;arg rdi, rsi, rdx
string_copy:
	xor rax, rax
	.loop:
		mov cl, byte[rdi+rax]
		mov byte[rsi+rax], cl
		inc rax
		dec rdx
		cmp rdx, 0
		jle .end
		jmp .loop
	.end:
		mov rax, 0
		ret
	

