ASM = nasm
ASMFLAGS = -felf64 -g

main.o:	main.asm dict.o lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o:	lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o:	dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

main: 	main.o lib.o dict.o
	ld -o $@ $^
